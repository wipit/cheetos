<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Submission extends Model
{
    protected $guarded = [];

    public function prize()
    {
        return $this->belongsTo('App\Prize');
    }

    public function delivery()
    {
        return $this->hasOne('App\Delivery');
    }
}
