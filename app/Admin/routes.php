<?php

use Illuminate\Routing\Router;

Admin::registerAuthRoutes();

Route::group([
    'prefix'        => config('admin.route.prefix'),
    'namespace'     => config('admin.route.namespace'),
    'middleware'    => config('admin.route.middleware'),
], function (Router $router) {
    $router->get('/', 'HomeController@index');
    $router->resource('/prizes', PrizeController::class);
    $router->resource('/delivery', DeliveryController::class);
    $router->resource('/submissions', SubmissionController::class);
    $router->resource('/users', UserController::class);
});
