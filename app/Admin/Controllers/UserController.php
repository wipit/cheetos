<?php

namespace App\Admin\Controllers;

use App\Submission;

use Encore\Admin\Form;
use Encore\Admin\Grid;
use Encore\Admin\Facades\Admin;
use Encore\Admin\Layout\Content;
use App\Http\Controllers\Controller;
use Encore\Admin\Controllers\ModelForm;

class UserController extends Controller
{
    use ModelForm;

    /**
     * Index interface.
     *
     * @return Content
     */
    public function index()
    {
        return Admin::content(function (Content $content) {

            $content->header('Submissions');
            $content->description('List');

            $content->body($this->grid());
        });
    }

    /**
     * Edit interface.
     *
     * @param $id
     * @return Content
     */
    public function edit($id)
    {
        return Admin::content(function (Content $content) use ($id) {

            $content->header('Submissions');
            $content->description('Edit');

            $content->body($this->form()->edit($id));
        });
    }

    /**
     * Create interface.
     *
     * @return Content
     */
    // public function create()
    // {
    //     return Admin::content(function (Content $content) {

    //         $content->header('Submissions');
    //         $content->description('New');

    //         $content->body($this->form());
    //     });
    // }

    /**
     * Make a grid builder.
     *
     * @return Grid
     */
    protected function grid()
    {
        return Admin::grid(Submission::class, function (Grid $grid) {
            $grid->disableActions();
            $grid->disableRowSelector();
            $grid->model()->groupBy('email');
            $grid->disableCreateButton();
            $grid->disableExport();
            // $grid->id('ID')->sortable();
            $grid->first_name('First name');
            $grid->last_name('Last name');
            $grid->email('E-mail');
            // $grid->shape_img('Original image');
            // $grid->final_img('Final image');
            $grid->facebook();
            $grid->google();
            $grid->zip('Zip code');
            $grid->dob('Date of birth');
            $grid->snackperks_opt('Snackperks');
            $grid->filter(function($filter){
                $filter->where(function ($query) {
                    $query->where('first_name', 'like', "%{$this->input}%")
                        ->orWhere('last_name', 'like', "%{$this->input}%");
                }, 'Search by name');
                $filter->where(function ($query) {
                    $query->where('email', 'like', "%{$this->input}%");
                }, 'Search by email');
            });
        });
    }

    /**
     * Make a form builder.
     *
     * @return Form
     */
    protected function form()
    {
        return Admin::form(Submission::class, function (Form $form) {
            $form->display('id', 'ID');
            $form->display('first_name', 'First name');
            $form->display('last_name', 'Last name');
            $form->display('email', 'E-mail');
            $form->display('shape_img', 'Original image');
            $form->display('final_img', 'Final image');
            $form->display('title');
            $form->display('description');
            $form->display('category');
            $form->display('facebook');
            $form->display('google');
            $form->display('zip', 'Zip code');
            $form->display('dob', 'Date of birth');
            $form->display('agree_to_rules', 'Agree to rules');
            $form->display('snackperks_opt', 'Snackperks');
            $form->display('retailers_name', 'Retailer');
            $form->display('prize_id', 'Prize');
            $form->display('google');
            $form->display('created_at', 'Received');
        });
    }
}

