<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


class PagesController extends Controller
{
	public function index() {
    	return view('index');
    }

    public function contest($token = null) {
        $data = \App\Submission::where('token', $token)->first();
        if($token == '' || count($data) < 1) {
            return abort(403, 'Token error');
        }
        if($data->prize_id != null) {
            return abort(403, 'Already won');
        }
    	return view('contest')->with('data', $data);
    }

    public function prize() {
    	return view('prize');
    }

    public function soap() {
    	return "SOAP";
    }
}
