<?php

namespace App\Http\Controllers;

use Artisaninweb\SoapWrapper\SoapWrapper;
use App\Soap\Request\GetConversionAmount;
use App\Soap\Response\GetConversionAmountResponse;

class SoapController
{
  /**
   * @var SoapWrapper
   */
  protected $soapWrapper;

  /**
   * SoapController constructor.
   *
   * @param SoapWrapper $soapWrapper
   */
  public function __construct(SoapWrapper $soapWrapper)
  {
    $this->soapWrapper = $soapWrapper;
  }

  /**
   * Use the SoapWrapper
   */
  public function show() 
  {
    // $response = '';
    // $data = [
    //     'CurrencyFrom' => 'USD',
    //     'CurrencyTo'   => 'EUR',
    //     'RateDate'     => '2014-06-05',
    //     'Amount'       => '2000'
    // ];

    // $this->soapWrapper->add('Currency', function ($service) {
    //   $service
    //     ->wsdl('http://currencyconverter.kowabunga.net/converter.asmx?WSDL')
    //     ->trace(true);
    // });

    // $response = $this->soapWrapper->call('Currency.GetConversionAmount', [$data]);

    // var_dump($response->GetConversionAmountResult);
    // exit();

    $data = [
      'Action' => 'ConsumerInsertOrAddEntry',
      'YASubmissionRequest' => [
        'RequestHeader' => [
          'ClientID' => '881',
          'OfferID' => '181432',
          'SecurityToken' => '20B898F7-6906-F83F-40C1-9A3425CA9D50',
          'Action' => 'ConsumerInsertOrAddEntry',
          'VendorCode' => 'ProPac',
          'RecordCount' => '1',
          'ReturnIdsFlag' => false
        ],
        'UniqueField' => '123456', // ID PROPIO MIO
        'ConsumerData' => [
          'ExternalID' => 'tester@test.com',
          'EntryMethodID' => '3',
          'FirstName' => 'Test',
          'LastName' => 'Tester',
          'PostalCode' => '90210',
          'CountryCode' => 'US',
          'Email' => 'tester@test.com',
          'DateofBirth' => '01/01/1990',
          'DailyEntry' => [
            'EntryDate' => '2017-03-17T15:24:37.897Z' // DIA
          ],
        ],
      ]
    ];

    $this->soapWrapper->add('Kroger', function ($service) {
      $service
        ->wsdl('http://e-webservices.yastaging.com/SubmissionServices.asmx?WSDL')
        // ->header('Content-type','application/soap+xml; charset=utf-8')
        ->trace(true);
    });

    $response = $this->soapWrapper->call('Kroger.ProcessRequest', ['requestXml' => $data]);

    var_dump($response->ProcessRequestResult);
    exit;
  }
}