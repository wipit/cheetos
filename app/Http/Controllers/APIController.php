<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class APIController extends Controller
{
    public function kroger(Request $request) {
    	$mandatory = ['shape_img', 'title', 'description', 'category', 'first_name', 'last_name', 'email', 'facebook', 'google', 'zip', 'dob', 'snackperks_opt', 'agree_to_rules', 'retailers_name'];
    	$message = array();
    	$error = false;
    	foreach($mandatory as $value) {
    		if(!$request->$value) {
    			$error = true;
    			$message[] = $value." is missing";
    		} 
    	}
        if($request->shape_image  == '') {
            $error = true;
            $message[] = "shape_image cannot be null";
        }
        if($request->email  == '') {
            $error = true;
            $message[] = "email cannot be null";
        }
    	if(!$error) {
    		$submission = \App\Submission::create([
    			'first_name' => $request->first_name,
    			'last_name' => $request->last_name,
    			'email' => $request->email,
    			'facebook' => $request->facebook,
    			'google' => $request->google,
    			'zip' => $request->zip,
    			'shape_img' => $request->shape_img,
    			'title' => $request->title,
    			'description' => $request->description,
    			'category' => $request->category,
    			'dob' => $request->dob,
    			'snackperks_opt' => $request->snackperks_opt,
    			'agree_to_rules' => $request->agree_to_rules,
    			'retailers_name' => $request->retailers_name,
    			'token' => str_random(20)
    		]);
    		$response = ['error' => $error, 'token' => $submission->token];
    	} else {
    		$response = ['error' => $error, 'message' => $message];
    	}
    	return $response;
    }
}
