<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function send(Request $request) {
    	$data = ['first' => $request->first, 'last' => $request->last, 'email' => 'email@email.com', 'code' => $request->code];
    	$response = array();
    	$response['response'] = 'ok';
    	$response['info'] = $data;
    	return $response;
    }

    public function form() {
    	return view('form');
    }
}
