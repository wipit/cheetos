<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    protected $table = "deliveries";

    public function submission()
    {
        return $this->belongsTo('App\Submission');
    }
}
