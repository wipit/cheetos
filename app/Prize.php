<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prize extends Model
{
    public function submissions()
    {
        return $this->hasMany('App\Submission');
    }
}
