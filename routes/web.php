<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index')->name('index');
Route::get('/form', 'FormController@form')->name('form');
Route::post('/form', 'FormController@send')->name('send');
Route::get('/contest/{token?}', 'PagesController@contest')->name('contest');
Route::get('/prize', 'PagesController@prize')->name('prize');
Route::get('/soap', 'SoapController@show')->name('soap');
Route::prefix('api')->group(function () {
    Route::post('/kroger-submission', 'APIController@kroger')->name('kroger');
});