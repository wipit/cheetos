<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubmissionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('submissions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name', 50);
            $table->string('last_name', 50);
            $table->string('email', 70);
            $table->string('shape_img');
            $table->string('final_img')->nullable()->default(null);
            $table->string('title');
            $table->text('description');
            $table->string('category', 70);
            $table->string('facebook', 70);
            $table->string('google', 70);
            $table->string('zip', 10);
            $table->date('dob');
            $table->enum('snackperks_opt', ['Y', 'N']);
            $table->enum('agree_to_rules', ['Y', 'N']);
            $table->string('retailers_name', 40);
            $table->integer('prize_id')->nullable()->default(null);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('submissions');
    }
}
