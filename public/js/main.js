var currentUnit = "percentage"
var rotate = 0
var scale = 1
$( function() {
  $("#slider_rotation").slider({
    min: 0,
    max: 360,
    slide: function(event, ui) {
      $(this).slider('value', ui.value);
      rotate = ui.value;
      $("#picture").css('transform', 'rotate(' + rotate + 'deg) scale(' + scale + ')')
      updateText()
    }
  });
  $("#slider_size").slider({
    min: 1,
    max: 50,
    value: 10,
    slide: function(event, ui) {
      $(this).slider('value', ui.value);
      scale = ui.value / 10;
      $("#picture").css('transform', 'rotate(' + rotate + 'deg) scale(' + scale + ')')
      updateText()
    }
  });
  $("#picture").draggable({
    // containment: "#shape",
    drag: function(event, ui) {
      updateText()
    }
  })
  var ratio = $("#picture img").width() / $("#picture img").height();
  $("#picture").resizable({
    // containment: "#shape",
    aspectRatio: ratio,
    resize: function(event, ui) {
      updateText()
    }
  })
  updateText();
});

function getPixelDimensions() {
  precision = 100
  // Save current transform (rotation).
  originalTransform = $("#picture").css('transform')
  // Remove rotation to make sure position() is the CSS position, not the bounding rect position.
  $("#picture").css('transform', 'rotate(0deg)')
  position = $("#picture").position()
  // Restore rotation.
  $("#picture").css('transform', originalTransform)
  
  dim = {
    top: Math.round(position.top * precision) / precision,
    left: Math.round(position.left * precision) / precision,
    width: Math.round($("#picture")[0].clientWidth * precision) / precision,
    height: Math.round($("#picture")[0].clientHeight * precision) / precision
  }
  
  return dim;
}

function getPercentageDimensions() {
  precision=1000
  parentWidth = $("#picture").parent().width();
  parentHeight = $("#picture").parent().height();
  
  dim = getPixelDimensions();
  dim.top = Math.round(dim.top / parentHeight * precision) / precision;
  dim.left = Math.round(dim.left / parentWidth * precision) / precision;
  dim.width = Math.round(dim.width / parentWidth * precision) / precision;
  dim.height = Math.round(dim.height / parentHeight * precision) / precision;
  
  return dim;
}

function updateText() {
  if(currentUnit == "pixels") {
    dim = getPixelDimensions();
    sufix = "px"
  } else {
    dim = getPercentageDimensions();
    sufix = "%"
  }
  
  $(".d").remove()
  for(prop in dim) {
    $("#dimensions").append( "<div class='d'>" + prop + ": " + dim[prop] + sufix + "</div>");
  }
  
  $("#dimensions").append( "<div class='d'>rotate: " + $("#slider_rotation").slider("value") + "deg</div>");
   
}



function previewFile() {
  var preview = document.querySelector('img'); //selects the query named img
  var file    = document.querySelector('input[type=file]').files[0]; //sames as here
  var reader  = new FileReader();

  reader.onloadend = function () {
    preview.src = reader.result;
  }

  if (file) {
    reader.readAsDataURL(file); //reads the data as a URL
  } else {
    preview.src = "";
  }
}