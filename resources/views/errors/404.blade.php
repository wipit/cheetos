<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="Distribution" content="Global" />
    <meta name="Robots" content="all, index, follow" />
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="title" content="CHEETOS :: Win what you see" />
    <title>CHEETOS :: Win what you see</title>
    <link rel="icon" href="{{ asset('css/favicon.ico') }}" />
    <link href="https://fonts.googleapis.com/css?family=Londrina+Solid:300,400,900|Montserrat:300" rel="stylesheet">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet" />
</head>
<body class="j-n">
    <div id="app" class="d-flex flex-column justify-content-between oops">
        <div id="loading" class="d-flex justify-content-center align-items-center hidden">
            <img src="{{ asset('images/loading.gif') }}" />
        </div>
        <header class="container-fluid oops">
			<div class="row">
				<div class="w-100">
			    	<img id="logo" src="{{ asset('images/cheetos.png') }}" />
			    	<h2><strong>PAGE NOT FOUND</strong></h2>
		    	</div>
			</div>
		</header>
        <div id="content" class="diff container-fluid flex-grow-1 oops">
			<div class="container">
				<div class="row">
					<div class="col-12 text-center">
						<h3 class="title">Did you find a fun <br/>Cheetos shape?</h3>
						<p class="copy">Head over to <a href="http://www.cheetoswinwhatyousee.com">CheetosWinWhatYouSee.com</a> <br/>and learn how you could win up to $100k in prizes!</p>
					</div>
				</div>
				<br/><br/><br/><br/>
			</div>
	    </div>
        @include('partials.footer')
    </div>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    <script src="{{ asset('js/main.js') }}"></script>
</body>
</html>