@extends('layouts.app')

@section('content')
	<div id="content" class="container small flex-grow-1">
	<form>
		<div class="form-group d-flex justify-content-between mb-0">
			<input type="text" class="form-control ml-5 read" name="first" value="First Name" readonly="true" />
			<input type="text" class="form-control mr-5 read" name="last" value="Last Name" readonly="true" />
		</div>
		<div class="form-group">
			<input type="text" class="form-control" name="address1" placeholder="Address Line 1" />
		</div>
		<div class="form-group">
			<input type="text" class="form-control" name="address2" placeholder="Address Line 2" />
		</div>
		<div class="form-group d-flex justify-content-between">
			<input type="text" class="form-control mr-2 flexy" name="city" placeholder="City" />
			<input type="text" class="form-control mr-2 ml-2 flexy half" name="state" placeholder="State" />
			<input type="text" class="form-control ml-2 flexy" name="code" placeholder="Zip code" />
		</div>
		<div class="form-group">
			<input type="text" class="form-control" name="phone" placeholder="Phone number" />
		</div>
		<button id="submit" type="submit" class="btn btn-primary mx-auto">Send my swag!</button>
	</form>
    </div>
@endsection

@section('scripts')
	<script type="text/javascript">
		$("form").submit(function(e){
	        e.preventDefault();
	        $("#loading").removeClass("hidden");
	        axios.post('/form', {
			    first: $("[name='first']").val(),
			    last: $("[name='last']").val(),
			    address1: $("[name='address1']").val(),
			    address2: $("[name='address2']").val(),
			    city: $("[name='city']").val(),
			    state: $("[name='state']").val(),
			    code: $("[name='code']").val(),
			    phone: $("[name='phone']").val()
			}).then(function (response) {
				$("#loading").addClass("hidden");
			    if(response.data.response == 'ok') {
			    	console.log(response.data.info);
			    } else {
			    	console.log("error");
			    }
			});
	    });
	</script>
@endsection