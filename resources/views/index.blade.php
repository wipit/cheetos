@extends('layouts.app')

@section('content')
	<div id="content" class="container small flex-grow-1">
		<div class="container">
			<div class="row">
				<div class="col-12 col-lg-10 text-center text-lg-left">
					<h3 class="title">Did you find a fun <br/>Cheetos shape?</h3>
					<p class="copy">Head over to <a href="http://www.cheetoswinwhatyousee.com">CheetosWinWhatYouSee.com</a> <br/>and learn how you could win up to $100k in prizes!</p>
				</div>
			</div>
		</div>
    </div>
@endsection