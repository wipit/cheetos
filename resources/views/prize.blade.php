@extends('layouts.app')

@section('content')
	<div id="content" class="diff container-fluid flex-grow-1">
		<div class="frame">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-lg-6 padd">
						<div class="edit">
							<h4>Edit your <br/>CHEETOS shape</h4>
							<h5>zoom & crop</h5>
							<a id="prize" href="#" class="btn btn-primary mx-auto">Claim your prize</a>
						</div>
					</div>
					<div class="col-lg-6">
						<div id="shape">
							<div id="overflow">
								<div id="picture" class="ui-widget-content">
									<img src="{{ asset('images/yiya.jpg') }}" />
								</div>
							</div>
							<img id="final" src="{{ asset('images/final.png') }}" />
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>
    <h1>ROTATION</h1>
    <div id="slider_rotation" style="display:inline-block; width: 50%; margin: 10px 0 10px 10px;"></div>
    <h1>SIZE</h1>
    <div id="slider_size" style="display:inline-block; width: 50%; margin: 10px 0 10px 10px;"></div>
    <div id="dimensions"></div>
@endsection

@section('scripts')
	<script type="text/javascript">
		$("form").submit(function(e){
	        e.preventDefault();
	        $("#loading").removeClass("hidden");
	    });
	</script>
@endsection