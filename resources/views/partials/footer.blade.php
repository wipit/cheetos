<footer class="container-fluid">
	<div class="container small">
		<div class="row">
	    	<ul id="terms" class="d-flex justify-content-around">
	    		<li><a href="#" target="_blank">Official rules</a></li>
	    		<li><a href="#" target="_blank">Privacy policy</a></li>
	    		<li><a href="#" target="_blank">Terms of service</a></li>
	    	</ul>
		</div>
	</div>
</footer>