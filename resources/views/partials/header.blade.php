<header class="container-fluid {{\Request::route()->getName()}}">
	<div class="row">
		<div class="w-100">
	    	<img id="logo" src="{{ asset('images/cheetos.png') }}" />
	    	@if(\Request::route()->getName() != 'prize')
	    		<img id="arm" src="{{ asset('images/arm.png') }}" />
	    	@endif
	    	@if(\Request::route()->getName() == 'contest')
	    		<h2>Here’s your chance to <strong>win exclusive Cheetos Swag</strong>courtesy of your favorite grocery store</h2>
	    	@endif
	    	@if(\Request::route()->getName() == 'index')
	    		<h2>GET YOUR PAWS ON  <strong>sweet prizes!</strong></h2>
	    	@endif
	    	@if(\Request::route()->getName() == 'prize')
	    		<div class="container frame">
					<div class="row align-items-center">
						<div class="col-lg-6 padd">
							<figure class="framed">
								<div class="legend">
									<h4>You’ve won A<br/><strong>luggage tag!</strong></h4>
								</div>
							</figure>
						</div>
						<div class="col-lg-6">
							<div class="tag">
								<img src="{{ asset('images/tag.png') }}" />
							</div>
						</div>
					</div>
				</div>
	    	@endif
    	</div>
	</div>
</header>