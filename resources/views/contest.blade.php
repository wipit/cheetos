@extends('layouts.app')

@section('content')
	<div id="content" class="diff container-fluid flex-grow-1">
		<div class="frame">
			<div class="container">
				<div class="row align-items-center">
					<div class="col-lg-6 padd">
						<figure class="framed">
							<div class="legend" style="background: url('{{$data->shape_img}}') no-repeat center center; background-position: cover;">
								<!-- <h4>USER'S APPROVED SHAPE GOES HERE</h4> -->
							</div>
						</figure>
					</div>
					<div class="col-lg-6">
						<div class="data">
							<div class="letters">
								<div>
									<h6>{{$data->first_name}} {{$data->last_name}}</h6>
									<h6>{{$data->email}}</h6>
									<h6>Zipcode: {{$data->zip}}</h6>
								</div>
							</div>
						</div>
						<p>Your contact information was pulled from your submission at <a href="http://www.winwhatyousee.com" target="_blak">WinWhatYouSee.com</a></p>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-12">
					<a id="prize" href="#" class="btn btn-primary mx-auto">Let's see that prize!</a>
				</div>
			</div>
		</div>
    </div>
@endsection

@section('scripts')
	<script type="text/javascript">
		$("form").submit(function(e){
	        e.preventDefault();
	        $("#loading").removeClass("hidden");
	    });
	</script>
@endsection